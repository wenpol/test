<?php
require '../vendor/autoload.php';

use PhpHelper\Http\HttpRequest;
use App\Spider;

$host = 'http://yhxz521.com';

$files = file_get_contents('./坑女友.txt');
$files = explode("\n", $files);

foreach ($files as $kk => $url) {
    $imgUrl = '';
    $imgPreg = '/<img src=\'(.*?)\' /';
    $firstImgUrl = '';
    $id = '';
    $maxPage = '';
    $dirTitlePath = __DIR__ . '/坑女友/';
    $dirTitle = '';
    $dirTitlePreg = '/<h1>(.*?)<\/h1>/';
    echo date('Y-m-d H:i:s') . ' 第' . ($kk+1) . "开始下载". PHP_EOL;

    //$url = 'http://yhxz521.com/a/guochanzipai/2020/0427/6693.html';
    $start = strrpos($url,'/') + 1;
    $urlPath = substr($url, 0, $start);
    $id = str_replace('.html', '', substr($url, $start));
    $maxPagePreg = '/' . $id . '_(.*?)\./';

    $spider = new Spider();

    $response = HttpRequest::getInstance()->get($url);

    // 目录名
    if (preg_match_all($dirTitlePreg, $response['response'], $matches2)) {
        foreach ($matches2[0] as $k => $v) {
            if ($k == 0) {
                $title = str_replace('<h1>', '', $v);
                $title = str_replace('</h1>', '', $title);
                $dirTitle = $title;
                break;
            }

        }
    }
    // 目录名转码
    $dirTitle = iconv("gb2312","utf-8//IGNORE",$dirTitle);
    $dirTitle = $dirTitlePath . '/' . $dirTitle;
    $logPath = $dirTitle.'/log.txt';

    // 下载结果
    $data = array();

    // 第一张图
    if (preg_match_all($imgPreg,$response['response'], $matches )) {
        foreach ($matches[1] as $match) {
            $firstImgUrl = $host . $match;
            $data['html'] = $url;
            $data['imgurl'] =$firstImgUrl;
            // 下载第一张图
            $data['res'] = $spider->download_image($firstImgUrl, true, $dirTitle);
            file_put_contents($logPath, json_encode($data) . PHP_EOL, FILE_APPEND);
            break;
        }
    }


    // 最大页id
    if (preg_match_all($maxPagePreg, $response['response'], $matches3)) {
        $maxPage = (int)max($matches3[1]);
    }


    // 剩余的
    $allUrl = [];
    for ($i = 2; $i <= $maxPage; $i++) {
        $allUrl[] = $urlPath . $id . '_' . $i . '.html';
    }

    foreach ($allUrl as $k => $html) {
        $response2 = HttpRequest::getInstance()->get($html);
        $data['html'] = $html;

        // 图片路径
        if (preg_match_all($imgPreg,$response2['response'], $matches4 )) {
            foreach ($matches4[1] as $match) {
                $img = $host . $match;
                $data['imgurl'] =$img;
                $data['res'] = $spider->download_image($img, true, $dirTitle);
                break;
            }
        }
        $data['maxPage'] = $maxPage;
        file_put_contents($logPath, json_encode($data) . PHP_EOL, FILE_APPEND);
    }

    echo date('Y-m-d H:i:s') . ' 第' . ($kk+1) . "个下载已结束". PHP_EOL;
}
