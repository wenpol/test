<?php
namespace App;

/**
 * 下载图片
 * Class Spider
 */
class Spider
{

    private $code; // 状态码
    private $message; // 消息
    /**
     * 图片位置的根路径
     * @var string
     */
    private $base_path;

    /**
     * url 对应的路径
     * @var string
     */
    private $url_path;


    /**
     * @param        $url
     * @param string $path
     *
     * @return string
     * @throws Exception
     */
    public function downloadImage($url, $path = '1/')
    {
        $this->base_path = dirname(__DIR__) . '/pictures/' . $path; // 将传递的路径，主动拼接上根图片目录
        $this->url_path = '/pictures/' . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 对认证证书来源的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // 从证书中检查SSL加密算法是否存在

        $file = curl_exec($ch);
        curl_close($ch);
        if ($file == false) {
            //  图片下载失败
            $this->code = -1;
            $this->message = '图片下载失败';
            return false;
        }
        //  文件夹时需要添加 / 的
        if (substr($this->base_path, -1, 1) !== '/') {
            $this->base_path = $this->base_path . '/';
        }
        return $this->saveAsImage($url, $file);
    }


    /**
     * 保存图片并返回url
     *
     * @param $url
     * @param $file
     *
     * @return string
     * @throws Exception
     */
    private function saveAsImage($url, $file)
    {
        $extension = pathinfo($url, PATHINFO_EXTENSION); //  获取图片后缀
        $filename = uniqid(microtime(true)) . '.' . $extension; // 为图片生成唯一文件名

        //  如果文件夹不存在，则生成
        if (!file_exists($this->base_path)) {
            $make_path = mkdir($this->base_path, 0777, true);
            if (!$make_path) {
                $this->code = -2;
                $this->message = '保存图片时，创建文件夹';
                return false;
            }
        }

        $resource = fopen($this->base_path . $filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
        return 'http://' . $_SERVER['SERVER_NAME'] . $this->url_path . '/' . $filename;
    }

    /**
     * 获取 message
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * 获取正太吗
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 下载远程图片到本地
     *
     * @param string $url 远程文件地址
     * @param string $filename 保存后的文件名（为空时则为随机生成的文件名，否则为原文件名）
     * @param array $fileType 允许的文件类型
     * @param string $dirName 文件保存的路径（路径其余部分根据时间系统自动生成）
     * @param int $type 远程获取文件的方式
     * @return json 返回文件名、文件的保存路径
     * @author blog.snsgou.com
     */
    public function download_image($url, $fileName = '', $dirName, $fileType = array('jpg', 'gif', 'png'), $type = 1)
    {
        if ($url == '') {
            return false;
        }

        // 获取文件原文件名
        $defaultFileName = basename($url);

        // 获取文件类型
        $suffix = substr(strrchr($url, '.'), 1);
        if (!in_array($suffix, $fileType)) {
            return false;
        }

        // 设置保存后的文件名
        $fileName = $fileName == '' ? time() . rand(0, 9) . '.' . $suffix : $defaultFileName;

        // 获取远程文件资源
        if ($type) {
            $ch = curl_init();
            $timeout = 30;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $file = curl_exec($ch);
            curl_close($ch);
        } else {
            ob_start();
            readfile($url);
            $file = ob_get_contents();
            ob_end_clean();
        }

        // 设置文件保存路径
        //$dirName = $dirName . '/' . date('Y', time()) . '/' . date('m', time()) . '/' . date('d', time());
        //$dirName = $dirName . '/' . date('Ym', time());
        if (!file_exists($dirName)) {
            mkdir($dirName, 0777, true);
        }

        // 保存文件
        $res = fopen($dirName . '/' . $fileName, 'a');
        fwrite($res, $file);
        fclose($res);

        return array(
            'fileName' => $fileName,
            'saveDir' => $dirName
        );
    }
}